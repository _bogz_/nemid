from fastapi import FastAPI

from app.api import iamalive, genders, users, passwords, authentication
from app.db import engine, metadata, database

metadata.create_all(engine)

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(iamalive.router)
app.include_router(genders.router, prefix="/genders", tags=["genders"])
app.include_router(users.router, prefix="/users", tags=["users"])
app.include_router(passwords.router, prefix="/passwords", tags=["passwords"])
app.include_router(authentication.router, prefix="/auth", tags=["auth"])

import os

from sqlalchemy import (Column, DateTime, Integer, MetaData,
                        Boolean, String, ForeignKey, Table, create_engine)
from databases import Database

DATABASE_URL = os.getenv("DATABASE_URL")

# SQLAlchemy
engine = create_engine(DATABASE_URL)
metadata = MetaData()
genders = Table(
    "gender",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("label", String(50), nullable=False),
)

users = Table(
    "user",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("email", String(50), nullable=False),
    Column("cpr", String(50), nullable=False),
    Column("nemid", String(50), nullable=False),
    Column("created_at", DateTime, nullable=False),
    Column("modified_at", DateTime, nullable=False),
    Column("gender_id", Integer, ForeignKey("gender.id"), nullable=False)
)

passwords = Table(
    "password",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("created_at", DateTime, nullable=False),
    Column("password_hash", String(256), nullable=False),
    Column("is_valid", Boolean, nullable=False),
    Column("user_id", Integer, ForeignKey("user.id"), nullable=False),
)


# DB query builder
database = Database(DATABASE_URL)

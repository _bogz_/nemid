from app.api.services import user_service
from app.api.models.user import CreateUserSchema, UserSchema, UserDB
from fastapi import APIRouter, HTTPException
from typing import List

router = APIRouter()


@router.post("/", response_model=UserDB, status_code=201)
async def create_user(payload: CreateUserSchema):
    user_id = await user_service.create(payload)

    if user_id == 0:
        raise HTTPException(
            status_code=500,
            detail="Unable to create your gender"
        )
    response_object = await user_service.get(user_id)

    return response_object


@router.get("/{id}/", response_model=UserDB, status_code=200)
async def get_user(id: int):
    user = await user_service.get(id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user


@router.get("/{nemid}/nemid/", response_model=UserDB, status_code=200)
async def get_user_nemid(nemid: str):
    user = await user_service.get_nemid(nemid)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user


@router.get("/{id}/cpr/", response_model=UserDB, status_code=200)
async def get_user_cpr(cpr: str):
    user = await user_service.get_cpr(cpr)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return user


@router.get("/", response_model=List[UserDB], status_code=200)
async def get_all_users():
    users = await user_service.get_all()

    return users


@router.delete("/{id}/", status_code=204)
async def delete_user(id: int):
    await user_service.delete(id)


@router.put("/{id}/", response_model=UserDB,
            status_code=200)
async def update_user(id: int, payload: UserSchema):
    user = await user_service.get(id)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="user not found"
        )

    user_id = await user_service.update(id, payload)
    user = await user_service.get(user_id)
    return user

from app.api.services import gender_service
from app.api.models.gender import GenderDB, GenderSchema
from fastapi import APIRouter, HTTPException
from typing import List

router = APIRouter()


@router.post("/", response_model=GenderDB, status_code=201)
async def create_gender(payload: GenderSchema):
    gender_id = await gender_service.create(payload)

    if gender_id == 0:
        raise HTTPException(
            status_code=500,
            detail="Unable to create your gender"
        )

    response_object = {
        "id": gender_id,
        "label": payload.label
    }

    return response_object


@router.get("/{id}/", response_model=GenderDB, status_code=200)
async def get_gender(id: int):
    gender = await gender_service.get(id)
    if not gender:
        raise HTTPException(status_code=404, detail="Gender not found")

    return gender


@router.get("/", response_model=List[GenderDB], status_code=200)
async def get_all_genders():
    genders = await gender_service.get_all()

    return genders


@router.delete("/{id}/", status_code=204)
async def delete_gender(id: int):
    await gender_service.delete(id)


@router.put("/{id}/", response_model=GenderDB,
            status_code=200)
async def update_gender(id: int, payload: GenderSchema):
    gender = await gender_service.get(id)
    if not gender:
        raise HTTPException(
            status_code=404,
            detail="Gender not found"
        )

    gender_id = await gender_service.update(id, payload)

    response_object = {
        "id": gender_id,
        "label": payload.label
    }
    return response_object

from pydantic import BaseModel


class AuthenticationRequest(BaseModel):
    nemid: str
    password: str

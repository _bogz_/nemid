from pydantic import BaseModel


class GenderSchema(BaseModel):
    label: str


class GenderDB(GenderSchema):
    id: int

from pydantic import BaseModel
from datetime import datetime


class CreateUserSchema(BaseModel):
    cpr: str
    nemid: str
    email: str
    password: str
    gender_id: int


class UserSchema(BaseModel):
    cpr: str
    nemid: str
    email: str
    gender_id: int


class UserDB(UserSchema):
    created_at: datetime
    modified_at: datetime
    id: int

from pydantic import BaseModel
from datetime import datetime


class PasswordSchema(BaseModel):
    password_hash: str
    user_id: str


class ChangePasswordSchema(BaseModel):
    nemid: str
    old_password: str
    new_password: str


class ResetPasswordSchema(BaseModel):
    cpr: str
    password: str


class PasswordDB(PasswordSchema):
    created_at: datetime
    modified_at: datetime
    is_valid: bool
    id: int

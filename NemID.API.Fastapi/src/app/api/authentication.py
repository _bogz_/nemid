from app.api.services import authentication_service
from app.api.models.authentication import AuthenticationRequest
from app.api.models.user import UserDB
from fastapi import APIRouter, HTTPException

router = APIRouter()


@router.post("/", response_model=UserDB, status_code=201)
async def authenticate(payload: AuthenticationRequest):
    user = await authentication_service.authenticate(payload)

    if not user:
        raise HTTPException(
            status_code=404,
            detail="Unable to authenticate"
        )

    return user

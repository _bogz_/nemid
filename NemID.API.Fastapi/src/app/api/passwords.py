from app.api.services import password_service, user_service
from app.api.models.password import (ChangePasswordSchema,
                                     ResetPasswordSchema)
from fastapi import APIRouter, HTTPException

router = APIRouter()


@router.post("/reset", status_code=201)
async def password_reset(payload: ResetPasswordSchema):

    user = await user_service.get_cpr(payload.cpr)
    password_id = await password_service.reset_password(user["id"],
                                                        payload.password)
    if password_id == 0:
        raise HTTPException(
            status_code=500,
            detail="Unable to create reset your password..."
        )
    response_object = await password_service.get_password_id(password_id)
    return response_object


@router.get("/{id}/", status_code=200)
async def get(id: int):
    password = await password_service.get_password_id(id)
    if not password:
        raise HTTPException(
            status_code=404,
            detail="Unable to get your password..."
        )
    return password


@router.get("/{user_id}/user_id", status_code=200)
async def get_user_id(user_id: int):
    password = await password_service.get_password_userid(user_id)
    if not password:
        raise HTTPException(
            status_code=404,
            detail="Unable to get your password..."
        )
    return password


@router.get("/{nemid}/nemid", status_code=200)
async def get_nemid(nemid: str):
    password = await password_service.get_password_nemid(nemid)
    if not password:
        raise HTTPException(
            status_code=404,
            detail="Unable to get your password..."
        )
    return password


@router.post("/change", status_code=201)
async def password_change(payload: ChangePasswordSchema):

    password_id = await password_service.change_password(payload)
    if password_id == 0:
        raise HTTPException(
            status_code=500,
            detail="Unable to create change your password... " +
            "Maybe your old password is wrong?"
        )
    response_object = await password_service.get_password_id(password_id)
    return response_object

from fastapi import FastAPI

router = FastAPI()

@router.get("/")
async def i_am_alive():
    return {"message": "running on port 8002"}
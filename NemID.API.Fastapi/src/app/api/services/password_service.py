from app.db import passwords, database
from app.api.services import user_service
from app.api.models.password import ChangePasswordSchema
from datetime import datetime


async def create(password_hash: str, user_id: str):
    query = (passwords.insert().values(
        created_at=datetime.now(),
        is_valid=True,
        user_id=user_id,
        password_hash=password_hash
        )
    )
    return await database.execute(query=query)


async def get_password_id(id: id):
    query = passwords.select().where(id == passwords.c.id and
                                     passwords.c.is_valid is True)
    return await database.fetch_one(query=query)


async def get_password_userid(user_id: int):
    query = passwords.select().where(passwords.c.user_id == user_id and
                                     passwords.c.is_valid is True)
    return await database.fetch_one(query=query)


async def get_password_nemid(nemid: str):
    user = await user_service.get_nemid(nemid)
    if not user:
        raise Exception
    print(user)
    query = passwords.select().where(passwords.c.user_id == user["id"] and
                                     passwords.c.is_valid is True)
    return await database.fetch_one(query=query)


async def get_password_cpr(cpr: str):
    user = await user_service.get_cpr(cpr)

    query = passwords.select().where(passwords.c.user_id == user["id"] and
                                     passwords.c.is_valid is True)
    return await database.fetch_one(query=query)


async def reset_password(user_id: int, new_password: str):
    old_password = await get_password_userid(user_id)

    if not old_password:
        raise Exception

    old_password_query = (
        passwords
        .update()
        .where(passwords.c.user_id == old_password["user_id"] and
               passwords.c.is_valid is True)
        .values(is_valid=False)
        .returning(passwords.c.id)
    )
    await database.execute(query=old_password_query)

    query = (passwords.insert().values(
        created_at=datetime.now(),
        is_valid=True,
        user_id=user_id,
        password_hash=new_password
        )
    )
    return await database.execute(query=query)


async def change_password(payload: ChangePasswordSchema):
    old_password = await get_password_nemid(payload.nemid)

    if not old_password:
        raise Exception

    old_password_query = (
        passwords
        .update()
        .where(passwords.c.user_id == old_password["user_id"] and
               passwords.c.is_valid is True)
        .values(is_valid=False)
        .returning(passwords.c.id)
    )
    await database.execute(query=old_password_query)

    query = (passwords.insert().values(
        created_at=datetime.now(),
        is_valid=True,
        user_id=old_password["user_id"],
        password_hash=payload.new_password
        )
    )
    return await database.execute(query=query)


async def delete_password_id(id: int):
    query = passwords.delete().where(id == passwords.c.id)
    await database.execute(query=query)


async def delete_password_userid(user_id: int):
    query = passwords.delete().where(user_id == passwords.c.user_id)
    await database.execute(query=query)

from app.api.models.authentication import AuthenticationRequest
from app.api.services import password_service, user_service


async def authenticate(payload: AuthenticationRequest):
    user = await user_service.get_nemid(payload.nemid)
    password = await password_service.get_password_nemid(payload.nemid)

    if not user or not password:
        raise Exception

    if password["password_hash"] != payload.password:
        raise Exception

    return user

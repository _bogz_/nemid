from app.api.models.gender import GenderSchema
from app.db import genders, database


async def create(payload: GenderSchema):
    query = genders.insert().values(label=payload.label)
    return await database.execute(query=query)


async def get_all():
    query = genders.select()
    return await database.fetch_all(query=query)


async def get(id: int):
    query = genders.select().where(id == genders.c.id)
    return await database.fetch_one(query=query)


async def update(id: int, payload: GenderSchema):
    query = (
        genders
        .update()
        .where(id == genders.c.id)
        .values(label=payload.label)
        .returning(genders.c.id)
    )

    return await database.execute(query=query)


async def delete(id: int):
    query = genders.delete().where(id == genders.c.id)
    await database.execute(query=query)

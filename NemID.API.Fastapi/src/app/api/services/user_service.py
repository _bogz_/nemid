from app.api.models.user import CreateUserSchema, UserSchema
from app.db import users, database
from app.api.services import password_service, gender_service
from datetime import datetime


async def create(payload: CreateUserSchema):
    gender = await gender_service.get(payload.gender_id)

    if not gender:
        raise Exception("Unable to find needed gender...")

    query = users.insert().values(
            email=payload.email,
            cpr=payload.cpr,
            nemid=payload.nemid,
            created_at=datetime.now(),
            modified_at=datetime.now(),
            gender_id=payload.gender_id
        )
    user_id = await database.execute(query=query)
    await password_service.create(payload.password, user_id)

    return user_id


async def get_all():
    query = users.select()
    return await database.fetch_all(query=query)


async def get(id: int):
    query = users.select().where(id == users.c.id)
    return await database.fetch_one(query=query)


async def get_cpr(cpr: str):
    query = users.select().where(cpr == users.c.cpr)
    return await database.fetch_one(query=query)


async def get_nemid(nemid: str):
    query = users.select().where(nemid == users.c.nemid)
    return await database.fetch_one(query=query)


async def update(id: int, payload: UserSchema):
    gender = await gender_service.get(payload.gender_id)

    if not gender:
        raise Exception("Unable to do things")

    query = (
        users
        .update()
        .where(id == users.c.id)
        .values(gender_id=payload.gender_id, modified_at=datetime.now(),
                cpr=payload.cpr, nemid=payload.nemid, email=payload.email)
        .returning(users.c.id)
    )

    return await database.execute(query=query)


async def delete(id: int):
    await password_service.delete_password_userid(id)
    query = users.delete().where(id == users.c.id)
    await database.execute(query=query)

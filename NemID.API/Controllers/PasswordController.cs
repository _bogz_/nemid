using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;
using NemID.API.Services.PasswordService;

namespace NemID.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PasswordController : ControllerBase
    {
        private readonly IPasswordRepository _passwordRepository;
        
        public PasswordController(IPasswordRepository passwordRepository)
        {
            _passwordRepository = passwordRepository;
        }

        /// <summary>Create password</summary>
        /// <param name="request">Model</param>
        [HttpPost("create")]
        [ProducesResponseType(typeof(PasswordDto), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PasswordDto>> CreatePassword([FromBody, BindRequired] CreatePasswordRequest request)
        {
            try
            {
                var passwordDto = await _passwordRepository.CreatePasswordAsync(request);
                return Created(string.Empty, passwordDto);
            }
            catch (NullReferenceException e)
            {
                return BadRequest(e.ToString());
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(409, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }


        /// <summary>Reset password</summary>
        /// <param name="request">Model</param>
        [HttpPost("reset")]
        [ProducesResponseType(typeof(PasswordDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PasswordDto>> ResetPassword([FromBody, BindRequired] ResetPasswordRequest request)
        {
            try
            {
                var passwordDto = await _passwordRepository.ResetPasswordAsync(request);
                return Ok(passwordDto);
            }
            catch (NullReferenceException e)
            {
                return BadRequest(e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Change password</summary>
        /// <param name="request">Model</param>
        [HttpPost("change")]
        [ProducesResponseType(typeof(PasswordDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PasswordDto>> ChangePassword([FromBody, BindRequired] ChangePasswordRequest request)
        {
            try
            {
                var passwordDto = await _passwordRepository.ChangePasswordAsync(request);
                return Ok(passwordDto);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.ToString());
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.ToString());
            }
            catch (UnauthorizedAccessException e)
            {
                return StatusCode(401, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get active password for user</summary>
        /// <param name="userId">Model</param>
        [HttpGet("{userId}/active")]
        [ProducesResponseType(typeof(PasswordDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PasswordDto>> GetActivePassword([FromRoute, BindRequired] int userId)
        {
            try
            {
                if(userId == 0)
                {
                    throw new InvalidOperationException("The provided user id cannot be 0 or null");
                }

                var passwordDto = await _passwordRepository.GetActivePasswordAsync(userId);
                return Ok(passwordDto);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (KeyNotFoundException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get all passwords for user</summary>
        /// <param name="userId">Model</param>
        [HttpGet("{userId}/all")]
        [ProducesResponseType(typeof(PasswordDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PasswordDto>> GetAllPasswords([FromRoute, BindRequired] int userId)
        {
            try
            {
                if(userId == 0)
                {
                    throw new InvalidOperationException("The provided user id cannot be 0 or null");
                }

                var passwordDtos = await _passwordRepository.GetPasswordsAsync(userId);
                return Ok(passwordDtos);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
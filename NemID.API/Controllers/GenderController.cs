using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;
using NemID.API.Services.GenderService;

namespace NemID.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class GenderController : ControllerBase
    {
        private readonly IGenderRepository _genderRepository;

        public GenderController(IGenderRepository genderRepository)
        {
            _genderRepository = genderRepository;
        }

        /// <summary>Create gender</summary>
        /// <param name="request">Model</param>
        [HttpPost]
        [ProducesResponseType(typeof(GenderDto), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> CreateGender([FromBody, BindRequired] CreateGenderRequest request)
        {
            try
            {
                var genderDto = await _genderRepository.CreateGenderAsync(request);
                return Created(string.Empty, genderDto);
            }
            catch (NullReferenceException e)
            {
                return BadRequest(e);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(409, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get gender</summary>
        /// <param name="id">Model</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GenderDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> GetGender([FromRoute, BindRequired] int id)
        {
            try
            {
                if(id == 0)
                {
                    throw new InvalidOperationException("The provided id cannot be 0 or null");
                }

                var genderDto = await _genderRepository.GetGenderAsync(id);
                return Ok(genderDto);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get all genders</summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<GenderDto>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> GetAllGenders()
        {
            try
            {
                var genderDtos = await _genderRepository.GetAllGendersAsync();
                return Ok(genderDtos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Delete gender</summary>
        /// <param name="id">Model</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> DeleteGender([FromRoute, BindRequired] int id)
        {
             try
            {
                if(id == 0)
                {
                    throw new InvalidOperationException("The provided id cannot be 0 or null");
                }

                await _genderRepository.DeleteGenderAsync(id);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Update gender</summary>
        /// <param name="request">Model</param>
        [HttpPut()]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> UpdateGender([FromBody, BindRequired] UpdateGenderRequest request)
        {
            try
            {
                await _genderRepository.UpdateGenderAsync(request);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
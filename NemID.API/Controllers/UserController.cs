using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;
using NemID.API.Services.UserService;

namespace NemID.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>Create user</summary>
        /// <param name="request">Model</param>
        [HttpPost]
        [ProducesResponseType(typeof(UserDto), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> CreateUser([FromBody, BindRequired] CreateUserRequest request)
        {
            try
            {
                var userDto = await _userRepository.CreateUserAsync(request);
                return Created(string.Empty, userDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.ToString());
            }
            catch(NullReferenceException e)
            {
                return NotFound(e.ToString());
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(409, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

         /// <summary>Get all users</summary>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserDto>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> GetAllUsers()
        {
            try
            {
                var userDtos = await _userRepository.GetAllUsersAsync();
                return Ok(userDtos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Delete user</summary>
        /// <param name="id">Model</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> DeleteUser([FromRoute, BindRequired] int id)
        {
             try
            {
                if(id == 0)
                {
                    throw new InvalidOperationException("The provided id cannot be 0 or null");
                }

                await _userRepository.DeleteUserAsync(id);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Update user</summary>
        /// <param name="request">Model</param>
        [HttpPut]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<GenderDto>> UpdateUser([FromBody, BindRequired] UpdateUserRequest request)
        {
            try
            {
                await _userRepository.UpdateUserAsync(request);
                return NoContent();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get user</summary>
        /// <param name="id">Model</param>
        [HttpGet("id/{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> GetUser([FromRoute, BindRequired] int id)
        {
            try
            {
                if(id == 0)
                {
                    throw new InvalidOperationException("The provided id cannot be 0 or null");
                }

                var userDto = await _userRepository.GetUserAsync(id);
                return Ok(userDto);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>Get user by nemId</summary>
        /// <param name="id">Model</param>
        [HttpGet("nemId/{nemId}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> GetUserNemId([FromRoute, BindRequired] string nemId)
        {
            try
            {
                if(string.IsNullOrEmpty(nemId))
                {
                    throw new InvalidOperationException("The provided id cannot be 0 or null");
                }

                var userDto = await _userRepository.GetUserNemIdAsync(nemId);
                return Ok(userDto);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (NullReferenceException e)
            {
                return StatusCode(404, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
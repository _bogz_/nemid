using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;
using NemID.API.Services.AuthenticationService;

namespace NemID.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationRepository _authenticationRepository;

        public AuthenticationController(IAuthenticationRepository authenticationRepository)
        {
            _authenticationRepository = authenticationRepository;
        }

        /// <summary>Authorize</summary>
        /// <param name="request">Model</param>
        [HttpPost]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<UserDto>> Authenticate([FromBody, BindRequired] AuthenticationRequest request)
        {
            try
            {
                var userDto = await _authenticationRepository.AuthenticateAsync(request);
                return Ok(userDto);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.ToString());
            }
            catch (UnauthorizedAccessException e)
            {
                return StatusCode(401, e.ToString());
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
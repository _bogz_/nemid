using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using NemID.API.Domain.Models;
using NemID.API.Services.AuthenticationService;
using NemID.API.Services.GenderService;
using NemID.API.Services.PasswordService;
using NemID.API.Services.UserService;
using NemID.API.Utils.Configurations.Filters;
using AutoMapper;
using NemID.API.Utils.Configurations.Mapper;
using Newtonsoft.Json;

namespace NemID.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // swagger config
            services.AddSwaggerGen();
            services.AddControllers();

            // Automapper config
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Fluent validation config
            services.AddMvc(options =>
            {
                options.Filters.Add(new ValidationFilter());
            })
            .AddNewtonsoftJson(
                options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore; 
            })
            .AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblyContaining<Startup>();
            });

            // Database config
            services.AddDbContext<NemIdUsersDbContext>();

            // Repository config
            services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
            services.AddScoped<IGenderRepository, GenderRepository>();
            services.AddScoped<IPasswordRepository, PasswordRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI( _ => 
            {
                _.SwaggerEndpoint("/swagger/v1/swagger.json", "NemID.API");
            });

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

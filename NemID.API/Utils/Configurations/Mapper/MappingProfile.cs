using AutoMapper;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Models;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Helpers;

namespace NemID.API.Utils.Configurations.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateUserRequest, UserDto>();
            CreateMap<UpdateUserRequest, UserDto>();

            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<CreateGenderRequest, GenderDto>();
            CreateMap<UpdateGenderRequest, GenderDto>();

            CreateMap<Gender, GenderDto>();
            CreateMap<GenderDto, Gender>();

            CreateMap<CreatePasswordRequest, PasswordDto>()
                .ForMember(dest => dest.PasswordHash, 
                    opt => opt.MapFrom(_ => PasswordHelper.HashPassword(_.Password)));
            CreateMap<ResetPasswordRequest, PasswordDto>()
                .ForMember(dest => dest.PasswordHash, 
                    opt => opt.MapFrom(_ => PasswordHelper.HashPassword(_.Password)));
            CreateMap<ChangePasswordRequest, PasswordDto>()
                .ForMember(dest => dest.PasswordHash, 
                    opt => opt.MapFrom(_ => PasswordHelper.HashPassword(_.NewPassword)));

            CreateMap<Password, PasswordDto>();
            CreateMap<PasswordDto, Password>();
        }
    }
}
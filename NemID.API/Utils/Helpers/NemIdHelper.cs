using System;

namespace NemID.API.Utils.Helpers
{
    public static class NemIdHelper
    {
        public static string Generate(string cpr, string email)
        {
            var emailAscii = 0;
            var randomNumber = GenerateRandomNo();
            foreach (var item in email)
            {
                emailAscii += System.Convert.ToInt32(item);
            }
            return ((emailAscii + randomNumber)%1000000000).ToString();
        }

        private static int GenerateRandomNo()
        {   
            var _min = 100000000;
            var _max = 999999999;
            var _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
    }
}
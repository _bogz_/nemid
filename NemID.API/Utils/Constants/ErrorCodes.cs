namespace NemID.API.Utils.Constants
{
    public static class ErrorCodes
    {
        public const string BadRequest = "400";
        public const string Unauthorized = "401";
        public const string NotFound = "404";
    }
}
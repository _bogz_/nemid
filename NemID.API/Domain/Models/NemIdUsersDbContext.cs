using Microsoft.EntityFrameworkCore;

namespace NemID.API.Domain.Models
{
    public class NemIdUsersDbContext : DbContext
    {   
        private string _connectionString {get;} = "Data Source=./Domain/Database/nemid_users.sqlite";
        public DbSet<User> Users {get;set;}
        public DbSet<Gender> Genders {get;set;}
        public DbSet<Password> Passwords {get;set;}

        public NemIdUsersDbContext(DbContextOptions<NemIdUsersDbContext> options) : base(options)
        {}

        public NemIdUsersDbContext()
        {}

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite(_connectionString);
            }
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Password>()
                .HasOne(u => u.User)
                .WithMany(p => p.Passwords)
                .HasForeignKey(u => u.UserId);
            
            builder.Entity<Gender>()
                .HasMany(u => u.User)
                .WithOne(g => g.Gender)
                .HasForeignKey(u => u.GenderId);
        }
    }
}
using System;
using System.Collections.Generic;

namespace NemID.API.Domain.Models
{
    public class User
    {
        public int Id {get;set;}
        public string Email {get;set;}
        public string NemId {get;set;}
        public string Cpr {get;set;}
        public DateTimeOffset CreatedAt {get;set;}
        public DateTimeOffset ModifiedAt {get;set;}
        public int GenderId {get;set;}

        public Gender Gender {get;set;}
        public List<Password> Passwords {get;set;}
    }
}
using System.Collections.Generic;

namespace NemID.API.Domain.Models
{
    public class Gender 
    {
        public int Id {get;set;}
        public string Label {get;set;}
        public List<User> User {get;set;}
    }
}
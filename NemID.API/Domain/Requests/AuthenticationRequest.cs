namespace NemID.API.Domain.Requests
{
    public class AuthenticationRequest
    {
        public string NemId {get;set;}
        public string Password {get;set;}
    }
}
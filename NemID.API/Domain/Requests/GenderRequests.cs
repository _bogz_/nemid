namespace NemID.API.Domain.Requests
{
    public class CreateGenderRequest
    {
        public string Label {get;set;}
    }

    public class UpdateGenderRequest
    {
        public int Id {get;set;}
        public string Label {get;set;}
    }
}
using System;

namespace NemID.API.Domain.Requests
{
    public class CreatePasswordRequest
    {
        public int UserId {get;set;}
        public string Password {get;set;}
    }

    public class ChangePasswordRequest
    {
        public int UserId {get;set;}
        public string OldPassword {get;set;}
        public string NewPassword{get;set;}
    }

    public class ResetPasswordRequest
    {
        public int UserId {get;set;}
        public string Password {get;set;}
    }
}
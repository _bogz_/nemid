namespace NemID.API.Domain.Requests
{
    public class CreateUserRequest
    {
        public string Email {get;set;}
        public string Cpr {get;set;}
        public int GenderId {get;set;}
        public string Password {get;set;}
    }

    public class UpdateUserRequest
    {
        public int Id {get;set;}
        public string Email {get;set;}
        public string NemId {get;set;}
        public string Cpr {get;set;}
        public int GenderId {get;set;}
    }

}
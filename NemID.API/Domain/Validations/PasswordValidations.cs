using FluentValidation;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Constants;

namespace NemID.API.Domain.Validations
{
    public class ChangePasswordRequestValidator : AbstractValidator<ChangePasswordRequest>
    {
        public ChangePasswordRequestValidator()
        {
            RuleFor(_ => _.UserId)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [UserId].");
            
            RuleFor(_ => _.OldPassword)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide the [OldPassword].");
            
            RuleFor(_ => _.NewPassword)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide the [NewPassword].");
        }
    }

    public class ResetPasswordRequestValidator : AbstractValidator<ResetPasswordRequest>
    {
        public ResetPasswordRequestValidator()
        {
            RuleFor(_ => _.UserId)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [UserId].");
            
            RuleFor(_ => _.Password)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide the [Password].");
            
        }
    }
}
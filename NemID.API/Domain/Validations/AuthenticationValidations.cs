using FluentValidation;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Constants;

namespace NemID.API.Domain.Validations
{
    public class AuthenticationRequestValidator : AbstractValidator<AuthenticationRequest>
    {
        public AuthenticationRequestValidator()
        {
            RuleFor(_ => _.NemId)
                .NotNull()
                .NotEmpty()
                .Length(9)
                .Matches("^\\d{9}$")
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [NemId] for this authentication attempt");

            RuleFor(_ => _.Password)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [Password] for this authentication attempt");
        }
    }
}
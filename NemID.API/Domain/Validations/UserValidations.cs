using FluentValidation;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Constants;

namespace NemID.API.Domain.Validations
{
    public class CreateUserRequestValidator : AbstractValidator<CreateUserRequest>
    {
        public CreateUserRequestValidator()
        {
            RuleFor(_ => _.Email)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [Email] for this user");

            RuleFor(_ => _.Cpr)
                .NotNull()
                .NotEmpty()
                .Length(10)
                .Matches("^\\d{10}$")
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a valid 10 digit [Cpr] for this user");

            RuleFor(_ => _.GenderId)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [GenderId] for this user");

            RuleFor(_ => _.Password)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [Password] for this user");
        }

    public class UpdateUserRequestValidator : AbstractValidator<UpdateUserRequest>
    {
        public UpdateUserRequestValidator()
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [Id] for this user");

            RuleFor(_ => _.Email)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [Email] for this user");
            
            RuleFor(_ => _.NemId)
                .NotNull()
                .NotEmpty()
                .Length(9)
                .Matches("^\\d{9}$")
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a valid 9 digit [NemId] for this user");

            RuleFor(_ => _.Cpr)
                .NotNull()
                .NotEmpty()
                .Length(10)
                .Matches("^\\d{10}$")
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a valid 10 digit [Cpr] for this user");

            RuleFor(_ => _.GenderId)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [GenderId] for this user");
        }
    }
    }
}
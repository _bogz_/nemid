using FluentValidation;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Constants;

namespace NemID.API.Domain.Validations
{
    public class CreateGenderRequestValidator : AbstractValidator<CreateGenderRequest>
    {
        public CreateGenderRequestValidator()
        {
            RuleFor(_ => _.Label)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [Label] for this gender");
        }
    }

        public class UpdateGenderRequestValidator : AbstractValidator<UpdateGenderRequest>
    {
        public UpdateGenderRequestValidator()
        {
            RuleFor(_ => _.Id)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide an [Id] for this gender");

            RuleFor(_ => _.Label)
                .NotNull()
                .NotEmpty()
                .WithErrorCode(ErrorCodes.BadRequest)
                .WithMessage("Please provide a [Label] for this gender");
        }
    }
}
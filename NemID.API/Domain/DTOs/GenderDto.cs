namespace NemID.API.Domain.DTOs
{
    public class GenderDto
    {
        public int Id {get;set;}
        public string Label {get;set;}
    }
}
using System;

namespace NemID.API.Domain.DTOs
{
    public class UserDto
    {
        public int Id {get;set;}
        public string Email {get;set;}
        public string NemId {get;set;}
        public string Cpr {get;set;}
        public DateTimeOffset CreatedAt {get;set;}
        public DateTimeOffset ModifiedAt {get;set;}
        public int GenderId {get;set;}
    }
}
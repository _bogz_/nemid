using System;

namespace NemID.API.Domain.DTOs
{
    public class PasswordDto
    {
        public int Id {get;set;}
        public DateTimeOffset CreatedAt{get;set;}
        public bool IsValid{get;set;}
        public string PasswordHash{get;set;}
        public int UserId {get;set;}
    }
}
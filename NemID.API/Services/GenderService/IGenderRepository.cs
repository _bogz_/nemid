using System.Collections.Generic;
using System.Threading.Tasks;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;

namespace NemID.API.Services.GenderService
{
    public interface IGenderRepository
    {
        Task<GenderDto> CreateGenderAsync(CreateGenderRequest request);
        Task<GenderDto> GetGenderAsync(int id);
        Task<IEnumerable<GenderDto>> GetAllGendersAsync();
        Task UpdateGenderAsync(UpdateGenderRequest request);
        Task DeleteGenderAsync(int id);
    }
}
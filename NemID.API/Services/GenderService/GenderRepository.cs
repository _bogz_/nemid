using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Models;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Extensions;

namespace NemID.API.Services.GenderService
{
    public class GenderRepository : IGenderRepository
    {
        private readonly NemIdUsersDbContext _dbContext;
        private readonly IMapper _mapper;

        public GenderRepository(NemIdUsersDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<GenderDto> CreateGenderAsync(CreateGenderRequest request)
        {
            var genderDto = _mapper.Map<GenderDto>(await _dbContext
                    .Genders
                    .AsNoTracking()
                    .FirstOrDefaultAsync(_ => _.Label.Equals(request.Label))
            );

            if(genderDto != null)
            {
                throw new InvalidOperationException($"Gender [{request.Label}] already exist in the database...");
            }

            genderDto = _mapper.Map<GenderDto>(request);
            var gender = _mapper.Map<Gender>(genderDto);

            await _dbContext.AddAsync(gender);
            await _dbContext.SaveChangesAsync();

            genderDto.Id = gender.Id;
            return genderDto;
        }

        public async Task DeleteGenderAsync(int id)
        {
            var gender = await _dbContext
                .Genders
                .Include(_ => _.User) 
                .FirstOrDefaultAsync(_ => _.Id == id);

            gender.ThrowIfNull(id);

            if(gender.User.Count != 0)
            {
                throw new InvalidOperationException("Unable to delete a gender that is still referenced in the User Table.");
            }

            _dbContext.Genders.Remove(gender);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<GenderDto>> GetAllGendersAsync()
        {
            return _mapper.Map<IEnumerable<GenderDto>>(await _dbContext
                .Genders
                .AsNoTracking()
                .ToListAsync()
                );
        }

        public async Task<GenderDto> GetGenderAsync(int id)
        {
            var genderDto = _mapper.Map<GenderDto>(await _dbContext
                .Genders
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Id == id)
            );

           genderDto.ThrowIfNull(id);

            return genderDto;
        }

        public async Task UpdateGenderAsync(UpdateGenderRequest request)
        {
            var gender = await _dbContext
                .Genders
                .FirstOrDefaultAsync(_ => _.Id == request.Id);
                
            gender.ThrowIfNull(request.Id);
            
            var activeGender = await _dbContext
                .Genders
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Label == request.Label);

            if(activeGender != null && activeGender.Id == request.Id)
            {
                throw new InvalidOperationException($"Cannot update the current gender to [{request.Label}]. It already exists");
            }

            gender.Label = request.Label;

            await _dbContext.SaveChangesAsync();
        }
    }
}
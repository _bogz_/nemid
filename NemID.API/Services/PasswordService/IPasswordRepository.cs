using System.Collections.Generic;
using System.Threading.Tasks;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;

namespace NemID.API.Services.PasswordService
{
    public interface IPasswordRepository
    {
        Task<PasswordDto> CreatePasswordAsync(CreatePasswordRequest request);
        Task<PasswordDto> ChangePasswordAsync(ChangePasswordRequest request);
        Task<PasswordDto> ResetPasswordAsync(ResetPasswordRequest request);
        Task<PasswordDto> GetActivePasswordAsync(int userId);
        Task<IEnumerable<PasswordDto>> GetPasswordsAsync(int userId);
    }
}
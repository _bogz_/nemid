using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Models;
using NemID.API.Domain.Requests;
using NemID.API.Utils.Extensions;
using NemID.API.Utils.Helpers;

namespace NemID.API.Services.PasswordService
{
    public class PasswordRepository : IPasswordRepository
    {
        private readonly NemIdUsersDbContext _dbContext;
        private readonly IMapper _mapper;

        public PasswordRepository(NemIdUsersDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<PasswordDto> ChangePasswordAsync(ChangePasswordRequest request)
        {
            var activePassword = await _dbContext
                .Passwords
                .FirstOrDefaultAsync(_ => _.UserId == request.UserId && _.IsValid == true);
                
            activePassword.ThrowIfNull(request.UserId);

            var result = PasswordHelper.VerifyPassword(request.OldPassword, activePassword.PasswordHash);
            if(!result)
            {
                throw new UnauthorizedAccessException($"The invalid old password for user [{request.UserId}]");
            }

            var passwordDto = _mapper.Map<PasswordDto>(request);
            passwordDto.CreatedAt = DateTime.Now.ToUniversalTime();
            passwordDto.IsValid = true;

            var password = _mapper.Map<Password>(passwordDto);
            activePassword.IsValid = false;

            await _dbContext.Passwords.AddAsync(password);
            await _dbContext.SaveChangesAsync();
            passwordDto.Id = password.Id;

            return passwordDto;
        }

        public async Task<PasswordDto> CreatePasswordAsync(CreatePasswordRequest request)
        {
            var passwordsForUser = await _dbContext
                .Passwords
                .AsNoTracking()
                .Where(_ => _.UserId == request.UserId && _.IsValid == true)
                .ToListAsync();
            
            if(passwordsForUser.Count != 0)
            {
                throw new InvalidOperationException($"The user with id [{request.UserId}] already has created a password.");
            }

            var user = await _dbContext.Users.FirstOrDefaultAsync(_ => _.Id == request.UserId);
            user.ThrowIfNull(request.UserId);

            var passwordDto = _mapper.Map<PasswordDto>(request);
            passwordDto.CreatedAt = DateTime.Now.ToUniversalTime();
            passwordDto.IsValid = true;

            var password = _mapper.Map<Password>(passwordDto);
            await _dbContext.Passwords.AddAsync(password);
            await _dbContext.SaveChangesAsync();

            passwordDto.Id = password.Id;
            return passwordDto;
        }

        public async Task<PasswordDto> GetActivePasswordAsync(int userId)
        {
            var passwordDto = _mapper.Map<PasswordDto>(await _dbContext
                .Passwords
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.UserId == userId && _.IsValid == true)
            );

            passwordDto.ThrowIfNull(userId);

            return passwordDto;
        }

        public async Task<IEnumerable<PasswordDto>> GetPasswordsAsync(int userId)
        {
            var passwordDtos = _mapper.Map<List<PasswordDto>>(await _dbContext
                .Passwords
                .AsNoTracking()
                .Where(_ => _.UserId == userId)
                .ToListAsync());
            
            if(passwordDtos.Count == 0)
                throw new NullReferenceException($"No passwords for user [{userId}] have been found.");
            
            return passwordDtos;
        }

        public async Task<PasswordDto> ResetPasswordAsync(ResetPasswordRequest request)
        {
            var activePasswords = await _dbContext
                .Passwords
                .Where(_ => _.UserId == request.UserId && _.IsValid == true)
                .ToListAsync();
            
            var user = await _dbContext.Users.FirstOrDefaultAsync(_ => _.Id == request.UserId);
            user.ThrowIfNull(request.UserId);
                
            var passwordDto = _mapper.Map<PasswordDto>(request);
            passwordDto.CreatedAt = DateTime.Now.ToUniversalTime();
            passwordDto.IsValid = true;

            var password = _mapper.Map<Password>(passwordDto);
            activePasswords.ForEach(_ => _.IsValid = false);

            await _dbContext.Passwords.AddAsync(password);
            await _dbContext.SaveChangesAsync();
            passwordDto.Id = password.Id;

            return passwordDto;
        }
    }
}
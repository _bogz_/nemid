using System.Collections.Generic;
using System.Threading.Tasks;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;

namespace NemID.API.Services.UserService
{
    public interface IUserRepository
    {
        Task<UserDto> CreateUserAsync(CreateUserRequest request);
        Task<UserDto> GetUserAsync(int userId);
        Task<UserDto> GetUserNemIdAsync(string nemId);
        Task<IEnumerable<UserDto>> GetAllUsersAsync();
        Task UpdateUserAsync(UpdateUserRequest request);
        Task DeleteUserAsync(int id);
    }
}
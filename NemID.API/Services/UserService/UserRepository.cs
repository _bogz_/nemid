using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Models;
using NemID.API.Domain.Requests;
using NemID.API.Services.GenderService;
using NemID.API.Services.PasswordService;
using NemID.API.Utils.Extensions;
using NemID.API.Utils.Helpers;

namespace NemID.API.Services.UserService
{
    public class UserRepository : IUserRepository
    {
        private readonly NemIdUsersDbContext _dbContext;
        private readonly IPasswordRepository _passwordRepository;
        private readonly IGenderRepository _genderRepository;
        private readonly IMapper _mapper;
        
        public UserRepository(NemIdUsersDbContext dbContext, IPasswordRepository passwordRepository, IGenderRepository genderRepository, IMapper mapper)
        {
            _dbContext = dbContext;
            _passwordRepository = passwordRepository;
            _genderRepository = genderRepository;
            _mapper = mapper;
        }

        public async Task<UserDto> CreateUserAsync(CreateUserRequest request)
        {
            var userDto = _mapper.Map<UserDto>(await _dbContext
                .Users
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Cpr == request.Cpr));
            
            if(userDto != null)
            {
                throw new InvalidOperationException($"A user with cpr [{request.Cpr}] already exists.");
            }

            var genderDto = await _genderRepository.GetGenderAsync(request.GenderId);

            userDto = _mapper.Map<UserDto>(request);
            userDto.CreatedAt = DateTime.Now.ToUniversalTime();
            userDto.ModifiedAt = DateTime.Now.ToUniversalTime();
            userDto.NemId = NemIdHelper.Generate(request.Cpr, request.Email);

            var user = _mapper.Map<User>(userDto);

            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();

            await _passwordRepository.CreatePasswordAsync(new CreatePasswordRequest{UserId = user.Id, Password = request.Password});
            userDto.Id = user.Id;
            return userDto;
        }

        public async Task DeleteUserAsync(int id)
        {
            var user = await _dbContext
                .Users
                .FirstOrDefaultAsync(_ => _.Id == id);
                
            user.ThrowIfNull(id);
            
            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserDto>> GetAllUsersAsync()
        {
            return _mapper.Map<List<UserDto>>(await _dbContext
                .Users
                .AsNoTracking()
                .ToListAsync());
        }

        public async Task<UserDto> GetUserAsync(int userId)
        {
            var userDto = _mapper.Map<UserDto>(await _dbContext
                .Users
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Id == userId));
            
            userDto.ThrowIfNull(userId);
            return userDto;
        }

        public async Task<UserDto> GetUserNemIdAsync(string nemId)
        {
            var userDto = _mapper.Map<UserDto>(await _dbContext
                .Users
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.NemId.Equals(nemId))
                );
                userDto.ThrowIfNull(nemId);
            return userDto;
        }

        public async Task UpdateUserAsync(UpdateUserRequest request)
        {
            var userWithSameCpr = await _dbContext
                .Users
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Cpr == request.Cpr);
            
            if(userWithSameCpr != null)
            {
                throw new InvalidOperationException($"A user with cpr [{request.Cpr}] already exists.");
            }

            var genderDto = await _genderRepository.GetGenderAsync(request.GenderId);

            var user = await _dbContext
                .Users
                .AsNoTracking()
                .FirstOrDefaultAsync(_ => _.Id == request.Id);

            user.ThrowIfNull(request.Id);

            var us = new User
            {
                Id = user.Id,
                Email = request.Email,
                Cpr = request.Cpr,
                GenderId = request.GenderId,
                CreatedAt = user.CreatedAt,
                ModifiedAt = DateTime.Now.ToUniversalTime()
            };

            _dbContext.Users.Update(us);

            await _dbContext.SaveChangesAsync();
        }
    }
}
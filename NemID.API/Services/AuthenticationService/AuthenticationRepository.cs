using System;
using System.Threading.Tasks;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;
using NemID.API.Services.PasswordService;
using NemID.API.Services.UserService;
using NemID.API.Utils.Helpers;

namespace NemID.API.Services.AuthenticationService
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordRepository _passwordRepository;


        public AuthenticationRepository(IUserRepository userRepository, IPasswordRepository passwordRepository)
        {
            _passwordRepository = passwordRepository;
            _userRepository = userRepository;
        }

        public async Task<UserDto> AuthenticateAsync(AuthenticationRequest request)
        {
            var userDto = await _userRepository.GetUserNemIdAsync(request.NemId);
            var passwordDto = await _passwordRepository.GetActivePasswordAsync(userDto.Id);

            var result = PasswordHelper.VerifyPassword(request.Password, passwordDto.PasswordHash);
            if(result)
            {
                return userDto;
            }
            throw new UnauthorizedAccessException("The provided credentials are not corresponding to an active user");
        }
    }
}
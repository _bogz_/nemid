using System.Threading.Tasks;
using NemID.API.Domain.DTOs;
using NemID.API.Domain.Requests;

namespace NemID.API.Services.AuthenticationService
{
    public interface IAuthenticationRepository
    {
        Task<UserDto> AuthenticateAsync(AuthenticationRequest request);
    }
}